#!/bin/bash -

set -x -o nounset                              # Treat unset variables as an error

phpize --clean && phpize && make clean

phpize
aclocal && libtoolize --force && autoreconf
./configure "$@" #--with-example=/home/ruslan/projects/lessons-sharedlib
