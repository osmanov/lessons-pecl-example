dnl Copyright © 2015 Ruslan Osmanov <rrosmanov@gmail.com> 

PHP_ARG_WITH(example, for example support,
[  --with-example             Include example support])

dnl Otherwise use enable:

dnl PHP_ARG_ENABLE(example, whether to enable example support,
dnl    [  --enable-example           Enable example support])

if test "$PHP_EXAMPLE" != "no"; then
   dnl with-example -> check with-path
   SEARCH_PATH="/usr/local /usr /opt /opt/local"
   SEARCH_FOR="/include/example.h"
   if test -r $PHP_EXAMPLE/$SEARCH_FOR; then
     EXAMPLE_DIR=$PHP_EXAMPLE
   else
     AC_MSG_CHECKING([for example files in default path])
     for i in $SEARCH_PATH ; do
       if test -r $i/$SEARCH_FOR; then
         EXAMPLE_DIR=$i
         AC_MSG_RESULT(found in $i)
       fi
     done
   fi

   if test -z "$EXAMPLE_DIR"; then
     AC_MSG_RESULT([not found])
     AC_MSG_ERROR([Please reinstall the example distribution])
   fi

   PHP_ADD_INCLUDE($EXAMPLE_DIR/include)

   LIBNAME=example
   LIBSYMBOL=e_stat

   PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
   [
     PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $EXAMPLE_DIR/$PHP_LIBDIR, EXAMPLE_SHARED_LIBADD)
     AC_DEFINE(HAVE_EXAMPLELIB,1,[ ])
   ],[
     AC_MSG_ERROR([wrong example lib version or lib not found])
   ],[
     -L$EXAMPLE_DIR/$PHP_LIBDIR -lm
   ])

   PHP_SUBST(EXAMPLE_SHARED_LIBADD)

  PHP_NEW_EXTENSION(example, example.c, $ext_shared)
fi
