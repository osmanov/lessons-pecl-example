<?php
$br = (php_sapi_name() == "cli")? "":"<br>";

if(!extension_loaded('example')) {
	//dl('example.' . PHP_SHLIB_SUFFIX);
  die("extension 'example' is not loaded\n");
}
$module = 'example';
$functions = get_extension_funcs($module);
echo "Functions available in the test extension:$br\n";
foreach($functions as $func) {
    echo $func."$br\n";
}
echo "$br\n";

if (!extension_loaded($module)) {
  fprintf(stderr, "Module $module is not compiled into PHP");
  exit(1);
}

print_r(example_stat(__FILE__));
