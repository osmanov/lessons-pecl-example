/* Copyright © 2015 Ruslan Osmanov <rrosmanov@gmail.com> */

#ifndef PHP_EXAMPLE_H
#define PHP_EXAMPLE_H

extern zend_module_entry example_module_entry;
#define phpext_example_ptr &example_module_entry

#define PHP_EXAMPLE_VERSION "0.1.0" /* Replace with version number for your extension */

#ifdef PHP_WIN32
#	define PHP_EXAMPLE_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_EXAMPLE_API __attribute__ ((visibility("default")))
#else
#	define PHP_EXAMPLE_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

PHP_MINIT_FUNCTION(example);
PHP_MSHUTDOWN_FUNCTION(example);
PHP_RINIT_FUNCTION(example);
PHP_RSHUTDOWN_FUNCTION(example);
PHP_MINFO_FUNCTION(example);

PHP_FUNCTION(example_stat);

/*
  	Declare any global variables you may need between the BEGIN
	and END macros here:

ZEND_BEGIN_MODULE_GLOBALS(example)
	long  global_value;
	char *global_string;
ZEND_END_MODULE_GLOBALS(example)
*/

/* In every utility function you add that needs to use variables
   in php_example_globals, call TSRMLS_FETCH(); after declaring other
   variables used by that function, or better yet, pass in TSRMLS_CC
   after the last function argument and declare your utility function
   with TSRMLS_DC after the last declared argument.  Always refer to
   the globals in your function as EXAMPLE_G(variable).  You are
   encouraged to rename these macros something shorter, see
   examples in any other php module directory.
*/

#ifdef ZTS
#define EXAMPLE_G(v) TSRMG(example_globals_id, zend_example_globals *, v)
#else
#define EXAMPLE_G(v) (example_globals.v)
#endif

#endif	/* PHP_EXAMPLE_H */

