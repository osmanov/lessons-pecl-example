# About

This is a sample PHP extension wrapping [`libexample`](https://bitbucket.org/osmanov/lessons-sharedlib) library.

This extension is of no practical use as it is written for the sole purpose of education.

# Steps

This section describes steps of the extension development.

## Step 1: create skeleton

Download PHP source, check the MD5 digest and unpack the archive:

```
$ mkdir -p ~/opt
$ cd ~/opt
$ wget -qO php-5.5.22.tar.bz2 http://ru2.php.net/get/php-5.5.22.tar.bz2/from/this/mirror
$ echo 'cd5a6321d71897dec26e29e795926669 php-5.5.22.tar.bz2' | md5sum -c
php-5.5.22.tar.bz2: OK
$ tar xjvf php-5.5.22.tar.bz2
$ cd php-5.5.22
$ ext/ext_skel --extname=example --skel=ext/skeleton
Creating directory example
Creating basic files: config.m4 config.w32 .svnignore example.c php_example.h CREDITS EXPERIMENTAL tests/001.phpt example.php [done].

To use your new extension, you will have to execute the following steps:

1.  $ cd ..
2.  $ vi ext/example/config.m4
3.  $ ./buildconf
4.  $ ./configure --[with|enable]-example
5.  $ make
6.  $ ./sapi/cli/php -f ext/example/example.php
7.  $ vi ext/example/example.c
8.  $ make

Repeat steps 3-6 until you are satisfied with ext/example/config.m4 and
step 6 confirms that your module is compiled into PHP. Then, start writing
code and repeat the last two steps as often as necessary.
```

### Description of the skeleton files
The tool has generated the following files within `example` directory:

```text
▾ tests/
    001.phpt          Test file
  config.m4           M4 script for autotools (generates `configure, Makefile etc.`).
  config.w32          Configuration script for Windows (has the same purpose as `config.m4`).
  CREDITS
  example.c           C source.
  example.php         Example of extension usage in PHP.
  EXPERIMENTAL
  php_example.h       C header.
```

## Step 2: build PHP with PEAR

We'll need some PEAR utilities such as `phpize`, which are shipped with PHP.

If you already have `phpize` installed, just skip this step.

In *Gentoo* we can just install `dev-lang/php`. The PEAR scripts are installed by default.

To get `phpize` in *Ubuntu* install `php5-dev` package.

*OpenSUSE* offers `php5-devel` package.

Alternatively, we can build PHP manually:
```bash
./configure \
--prefix=$HOME/opt \
--mandir=$HOME/opt/share/man \
--infodir=$HOME/opt/share/info \
--datadir=$HOME/opt/share \
--sysconfdir=$HOME/opt/etc \
--localstatedir=$HOME/opt/var/lib \
--prefix=$HOME/opt/lib64/php5.5 \
--mandir=$HOME/opt/lib64/php5.5/man \
--infodir=$HOME/opt/lib64/php5.5/info \
--libdir=$HOME/opt/lib64/php5.5/lib \
--with-libdir=lib64 \
--disable-all \
--with-config-file-path=$HOME/opt/etc/php/cli-php5.5 \
--with-config-file-scan-dir=$HOME/opt/etc/php/cli-php5.5/ext-active
cores=$(grep -m1 'cores' /proc/cpuinfo | sed -e 's/[^0-9]//g' -)
((++cores))
make -j$cores
make install
mkdir -p $HOME/opt/etc/php/cli-php5.5/ext-active
```

Now all needed executables should be installed into `~/opt/bin` directory.

Let's prepend `~/opt/bin/php` to `$PATH`:


## Step 3: moving skeleton files to separate directory

In the previous step `ext/ext_skel` generated skeleton within the PHP source in
`example` directory. Let's make special directory for our project:
```bash
dir=~/projects/lessons
mkdir -p $dir
mv example $dir/pecl-example
cd $dir/pecl-example
```

Note: if you're using Git, then rename `.svnignore` file to `.gitignore`.

## Step 4: modifying skeleton for the first run

The `phpize` script generates traditional `configure` script according to `config.m4`.

For the first run we have to choose what kind of `configure` option we need:
whether `--enable-example`, or `--with-example=path`. The first version acts as
a flag. The second accepts path to some directory or file (more often path to
directory where the script should search for some library).

So we'll just uncommet the following:
```m4
PHP_ARG_ENABLE(example, whether to enable example support,
    [  --enable-example           Enable example support])
```

## Step 5: building and testing

Generate `configure` script.
```
phpize
```

Then follow traditional compilation steps:
```
./configure --enable-example
make
make test
```

Run `example.php`:
```
$ php -n -d extension=example.so -d extension_dir=.libs example.php
Functions available in the test extension:
confirm_example_compiled

Congratulations! You have successfully modified ext/example/config.m4. Module example is now compiled into PHP.
```

## Step 6: binding a shared library

Let's build and bind [our test library](https://bitbucket.org/osmanov/lessons-sharedlib) to the PHP extension.

Before linking against a library we have to know its path. Of course, we'll try automamic checks for some
common locations such as `/usr/include`, `/usr/lib`, `/usr/lib64`, `/usr/local/lib` etc. However, user might
want to install the library to his private directory.

So the path to `libexample` installation directory will be accepted as input. Thus, we have to apply the following
changes to `config.m4`:

a) remove previously uncommented block for `--enable-example`:
```m4
dnl PHP_ARG_ENABLE(example, whether to enable example support,
dnl    [  --enable-example           Enable example support])
```
b) handle `with-example=directory`:

```m4
PHP_ARG_WITH(example, for example support,
[  --with-example             Include example support])

dnl Otherwise use enable:

dnl PHP_ARG_ENABLE(example, whether to enable example support,
dnl    [  --enable-example           Enable example support])

if test "$PHP_EXAMPLE" != "no"; then
   dnl with-example -> check with-path
   SEARCH_PATH="/usr/local /usr /opt /opt/local"
   SEARCH_FOR="/include/example.h"
   if test -r $PHP_EXAMPLE/$SEARCH_FOR; then
     EXAMPLE_DIR=$PHP_EXAMPLE
   else
     AC_MSG_CHECKING([for example files in default path])
     for i in $SEARCH_PATH ; do
       if test -r $i/$SEARCH_FOR; then
         EXAMPLE_DIR=$i
         AC_MSG_RESULT(found in $i)
       fi
     done
   fi

   if test -z "$EXAMPLE_DIR"; then
     AC_MSG_RESULT([not found])
     AC_MSG_ERROR([Please reinstall the example distribution])
   fi

   PHP_ADD_INCLUDE($EXAMPLE_DIR/include)

   LIBNAME=example
   LIBSYMBOL=e_stat

   PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
   [
     PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $EXAMPLE_DIR/$PHP_LIBDIR, EXAMPLE_SHARED_LIBADD)
     AC_DEFINE(HAVE_EXAMPLELIB,1,[ ])
   ],[
     AC_MSG_ERROR([wrong example lib version or lib not found])
   ],[
     -L$EXAMPLE_DIR/$PHP_LIBDIR -lm
   ])

   PHP_SUBST(EXAMPLE_SHARED_LIBADD)
```

Now let's rebuild with new option:
```
phpize --clean
phpize
./configure --with-example=../libexample
```
(You might want to adjust the path to `libexample` installaction directory.)


## Step 7: adding `example_stat` PHP function

Let's include the library header into `example.c`:
```c
/* libexample's header */
#include "example.h"
```

To the bottom of `example.c` add implementation of the new function:

```c
#ifndef MAKE_LONG_ZVAL_INCREF
#define MAKE_LONG_ZVAL_INCREF(name, val) \
MAKE_STD_ZVAL(name); \
ZVAL_LONG(name, val); \
Z_ADDREF_P(name);
#endif

/* {{{ proto array example_stat(string path)
   Return an array with stat information on success, otherwise false. */
PHP_FUNCTION(example_stat)
{
	char *path = NULL;
	int path_len;

	zval *stat_dev, *stat_ino, *stat_mode, *stat_nlink, *stat_uid, *stat_gid, *stat_rdev,
		 *stat_size, *stat_atime, *stat_mtime, *stat_ctime, *stat_blksize, *stat_blocks;
	struct stat stat_sb;
	char *stat_sb_names[13] = {
		"dev", "ino", "mode", "nlink", "uid", "gid", "rdev",
		"size", "atime", "mtime", "ctime", "blksize", "blocks"
	};

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &path, &path_len) == FAILURE) {
		return;
	}

	if (e_stat(path, &stat_sb) == -1) {
		php_error_docref(NULL TSRMLS_CC, E_WARNING, "e_stat('%s') failed", path);
		RETURN_FALSE;
	}

	array_init(return_value);

	MAKE_LONG_ZVAL_INCREF(stat_dev, stat_sb.st_dev);
	MAKE_LONG_ZVAL_INCREF(stat_ino, stat_sb.st_ino);
	MAKE_LONG_ZVAL_INCREF(stat_mode, stat_sb.st_mode);
	MAKE_LONG_ZVAL_INCREF(stat_nlink, stat_sb.st_nlink);
	MAKE_LONG_ZVAL_INCREF(stat_uid, stat_sb.st_uid);
	MAKE_LONG_ZVAL_INCREF(stat_gid, stat_sb.st_gid);
#ifdef HAVE_ST_RDEV
	MAKE_LONG_ZVAL_INCREF(stat_rdev, stat_sb.st_rdev);
#else
	MAKE_LONG_ZVAL_INCREF(stat_rdev, -1);
#endif
	MAKE_LONG_ZVAL_INCREF(stat_size, stat_sb.st_size);
	MAKE_LONG_ZVAL_INCREF(stat_atime, stat_sb.st_atime);
	MAKE_LONG_ZVAL_INCREF(stat_mtime, stat_sb.st_mtime);
	MAKE_LONG_ZVAL_INCREF(stat_ctime, stat_sb.st_ctime);
#ifdef HAVE_ST_BLKSIZE
	MAKE_LONG_ZVAL_INCREF(stat_blksize, stat_sb.st_blksize);
#else
	MAKE_LONG_ZVAL_INCREF(stat_blksize,-1);
#endif
#ifdef HAVE_ST_BLOCKS
	MAKE_LONG_ZVAL_INCREF(stat_blocks, stat_sb.st_blocks);
#else
	MAKE_LONG_ZVAL_INCREF(stat_blocks,-1);
#endif
	/* Store numeric indexes in propper order */
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_dev, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_ino, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_mode, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_nlink, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_uid, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_gid, sizeof(zval *), NULL);

	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_rdev, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_size, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_atime, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_mtime, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_ctime, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_blksize, sizeof(zval *), NULL);
	zend_hash_next_index_insert(HASH_OF(return_value), (void *)&stat_blocks, sizeof(zval *), NULL);

	/* Store string indexes referencing the same zval*/
	zend_hash_update(HASH_OF(return_value), stat_sb_names[0], strlen(stat_sb_names[0])+1, (void *) &stat_dev, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[1], strlen(stat_sb_names[1])+1, (void *) &stat_ino, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[2], strlen(stat_sb_names[2])+1, (void *) &stat_mode, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[3], strlen(stat_sb_names[3])+1, (void *) &stat_nlink, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[4], strlen(stat_sb_names[4])+1, (void *) &stat_uid, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[5], strlen(stat_sb_names[5])+1, (void *) &stat_gid, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[6], strlen(stat_sb_names[6])+1, (void *) &stat_rdev, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[7], strlen(stat_sb_names[7])+1, (void *) &stat_size, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[8], strlen(stat_sb_names[8])+1, (void *) &stat_atime, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[9], strlen(stat_sb_names[9])+1, (void *) &stat_mtime, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[10], strlen(stat_sb_names[10])+1, (void *) &stat_ctime, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[11], strlen(stat_sb_names[11])+1, (void *) &stat_blksize, sizeof(zval *), NULL);
	zend_hash_update(HASH_OF(return_value), stat_sb_names[12], strlen(stat_sb_names[12])+1, (void *) &stat_blocks, sizeof(zval *), NULL);
}

```

Now add the prototype to `php_example.h`:

```c
PHP_FUNCTION(example_stat);
```

Rebuild the project.


## Step 8: testing `example_stat` PHP function

Add the following to the end of `example.php`:
```php
print_r(example_stat(__FILE__));
```

Let's make a shell script for running test scripts (`run.sh`):
```bash
#!/bin/bash -
php -n -d extension=example.so -d extension_dir=.libs "$@"
```

Make it executable:
```bash
chmod +x run.sh
```

Now run the test:
```bash
./run.sh example.php
```

If everything is okay, the output should be similar to the following:
```text
Functions available in the test extension:
example_stat

Array
(
    [0] => 2057
    [1] => 1851882
    [2] => 33188
    [3] => 1
    [4] => 1000
    [5] => 1000
    [6] => 0
    [7] => 508
    [8] => 1425135582
    [9] => 1425135580
    [10] => 1425135580
    [11] => 4096
    [12] => 8
    [dev] => 2057
    [ino] => 1851882
    [mode] => 33188
    [nlink] => 1
    [uid] => 1000
    [gid] => 1000
    [rdev] => 0
    [size] => 508
    [atime] => 1425135582
    [mtime] => 1425135580
    [ctime] => 1425135580
    [blksize] => 4096
    [blocks] => 8
)
```
